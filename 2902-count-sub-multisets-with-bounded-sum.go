package main

import (
	"fmt"
	"sort"
)

type NumberRange struct {
	Start, Stop int
}

type NumberCount struct {
	Num, Count int
}

type DistinctRange map[int]NumberRange

func CountSubMultisets(nums []int, l int, r int) int {
	numLen := len(nums)
	if numLen == 0 {
		return 0
	}

	if l > r {
		return 0
	}

	sort.Ints(nums)
	fmt.Println(nums)

	if nums[0] > r {
		return 0
	}

	if numLen*nums[numLen-1] < l {
		return 0
	}

	ranges := make(DistinctRange)

	CurrentNum := nums[0]
	CurrentRange := NumberRange{0, 0}
	ranges[CurrentNum] = CurrentRange

	for i := 1; i < numLen; i++ {
		if nums[i] > r {
			numLen = i
			break
		}
		r := ranges[nums[i]]
		if CurrentNum == nums[i] {
			r.Stop = i
		} else {
			CurrentNum = nums[i]
			r = NumberRange{i, i}
		}
		ranges[CurrentNum] = r
	}

	fmt.Println(ranges)
	result := 0

	return result
}
