package main

import (
	"github.com/stretchr/testify/assert"
	"testing"
)

func TestCountSubMultisets(t *testing.T) {
	type args struct {
		nums []int
		l    int
		r    int
	}
	tests := []struct {
		name string
		args args
		want int
	}{
		{"Test case 0",
			args{[]int{1, 2, 2, 3}, 6, 6},
			1},
		{"Test case 1",
			args{[]int{2, 1, 4, 2, 7}, 1, 5},
			7},
		{"Test case 2",
			args{[]int{1, 2, 1, 3, 5, 2}, 3, 5},
			9},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			assert.Equalf(t, tt.want, CountSubMultisets(tt.args.nums, tt.args.l, tt.args.r), "CountSubMultisets(%v, %v, %v)", tt.args.nums, tt.args.l, tt.args.r)
		})
	}
}
