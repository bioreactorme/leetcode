package main

import (
	"github.com/stretchr/testify/assert"
	"testing"
)

func TestFindSubstring128(t *testing.T) {
	words := []string{"word", "good", "best", "good"}
	expected := []int{8}
	res := FindSubstring("wordgoodgoodgoodbestword", words)
	assert.Equal(t, expected, res)
}

func TestFindSubstring0(t *testing.T) {
	words := []string{"foo", "bar"}
	expected := []int{0, 9}
	res := FindSubstring("barfoothefoobarman", words)
	assert.Equal(t, expected, res)
}

func TestFindSubstring1(t *testing.T) {
	words := []string{"word", "good", "best", "word"}
	expected := []int{}
	res := FindSubstring("wordgoodgoodgoodbestword", words)
	assert.Equal(t, expected, res)
}
func TestFindSubstring2(t *testing.T) {
	words := []string{"bar", "foo", "the"}
	expected := []int{6, 9, 12}
	res := FindSubstring("barfoofoobarthefoobarman", words)
	assert.Equal(t, expected, res)
}
