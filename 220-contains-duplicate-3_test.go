package main

import (
	"github.com/stretchr/testify/assert"
	"testing"
)

func TestContainsNearbyAlmostDuplicate(t *testing.T) {
	assert.False(t, ContainsNearbyAlmostDuplicate([]int{1, 5, 9, 1, 5, 9}, 2, 3))
	assert.True(t, ContainsNearbyAlmostDuplicate([]int{1, 2, 3, 1}, 3, 0))
	assert.True(t, ContainsNearbyAlmostDuplicate([]int{1, 0, 1, 1}, 1, 2))
}
