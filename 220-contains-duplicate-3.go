package main

import "math"

func ContainsNearbyAlmostDuplicate(nums []int, k int, t int) bool {
	if k < 1 {
		return false
	}

	for wSz := 1; wSz <= k; wSz++ {
		ws := 0
		for we := ws + wSz; we < len(nums); we++ {
			if int(math.Abs(float64(nums[ws]-nums[we]))) <= t {
				return true
			}
			ws++
		}

	}

	return false
}
