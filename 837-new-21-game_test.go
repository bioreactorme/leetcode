package main

import (
	"github.com/stretchr/testify/assert"
	"testing"
)

func TestNew21Game(t *testing.T) {
	assert.Equal(t, 1.0, New21Game(10, 1, 10))
	assert.Equal(t, 0.6, New21Game(6, 1, 10))
	assert.Equal(t, 0.73278, New21Game(21, 17, 10))
}
