package main

import (
	"github.com/stretchr/testify/assert"
	"testing"
)

func TestMaxTotalFruits(t *testing.T) {

	assert.Equal(t, 9, MaxTotalFruits([][]int{{2, 8}, {6, 3}, {8, 6}}, 5, 4))
	assert.Equal(t, 14, MaxTotalFruits([][]int{{0, 9}, {4, 1}, {5, 7}, {6, 2}, {7, 4}, {10, 9}}, 5, 4))
	assert.Equal(t, 0, MaxTotalFruits([][]int{{0, 3}, {6, 4}, {8, 5}}, 3, 2))
	assert.Equal(t, 10000, MaxTotalFruits([][]int{{0, 10000}}, 200000, 200000))
	assert.Equal(t, 10000, MaxTotalFruits([][]int{{200000, 10000}}, 0, 200000))
	assert.Equal(t, 22, MaxTotalFruits([][]int{{1, 9}, {2, 10}, {3, 1}, {5, 6}, {6, 3}, {8, 2}, {9, 2}, {11, 4}, {18, 10}, {22, 8}, {25, 2}, {26, 2}, {30, 4}, {31, 5}, {33, 9}, {34, 1}, {39, 10}}, 19, 9))
	assert.Equal(t, 22, MaxTotalFruits([][]int{{0, 10}, {5, 1}, {6, 9}, {8, 5}, {9, 4}, {13, 2}, {14, 2}, {17, 8}, {21, 10}, {28, 4}, {30, 2}, {31, 2}, {33, 3}, {34, 6}, {36, 1}, {37, 10}, {38, 9}}, 20, 9))
	assert.Equal(t, 43, MaxTotalFruits([][]int{{3, 10}, {8, 6}, {11, 8}, {16, 5}, {17, 1}, {19, 2}, {24, 4}, {29, 4}, {30, 8}, {31, 1}, {33, 2}, {34, 9}, {35, 9}, {40, 10}}, 39, 13))
	assert.Equal(t, 128, MaxTotalFruits([][]int{{0, 6}, {2, 2}, {6, 7}, {9, 8}, {10, 10}, {12, 5}, {13, 5}, {14, 5}, {15, 6}, {18, 10}, {19, 7}, {20, 5}, {21, 8}, {23, 10}, {24, 8}, {29, 3}, {32, 9}, {34, 5}, {35, 7}, {37, 10}}, 9, 36))
}
